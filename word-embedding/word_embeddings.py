import numpy as np


class WordEmbedding:
    """ This class provides easy mechanism to extract the word vectors from a vectors file like GloVe.
    The expected file format is
        word x1 x2 x3 ...., where x1, x2 ... xn are the features, vector coordinates.

    Instead of loading the entire file in memory at once, this class lazily loads the file
    and  only extracts the word vectors of the requested words. This helps save memory when dealing
    with large files, as the word vectors are typically huge (~ 1 - 10GBs) """

    def __init__(self, vector_file, encoding):
        self.filepath = vector_file
        self.encoding = encoding

    def get_embedding(self):
        """ Open the file for reading the word vectors

        :return: generator object yielding word and word-vectors
        """
        for line in open(self.filepath, 'r', encoding=self.encoding):
            yield self.get_array(line)

    def get_array(self, line):
        """ Extract the word and word-embedding from a line of text. The line should be in the expected
        format.
            word x1, x2, x3 .... xn ,   where x1, x2 .. xn are the vector components.

        :param     line:
        :return:   word and numpy array containing the word vector
        """

        # TODO: Use spacy to tokenize
        line_arr = line.rstrip().split(' ')
        n_arr = np.array(list(map(np.float32, line_arr[1:])))
        return line_arr[0].encode(self.encoding), n_arr

    def load_word_vectors(self, words):
        """ Load the word vectors from word embedding file
        :param      words: array of words
        :return:    dictionary of word: vector
        """
        vocab = dict()
        for w, vec in self.get_embedding():
            if len(words) == 0:
                break;
            for word in words:
                if word.encode(self.encoding) == w:
                    vocab[w] = vec / np.linalg.norm(vec)
                    words.remove(word)
        return vocab


def main():
    """ Example usage of the class. Prints the similarity coefficient of
    all the words in a given sentence w.r.t a give set of words (array)

        1. Create a WordEmbedding object with file path and file encoding.
        2. Declare the text and reference words to which the similarity will
           computed.
        3. Take dot product of all the words in the text with the reference words,
           calculate the mean
        4. Print a table of words with the similarity coefficient.
        5. Print the most similar word. (Take the max)
    """
    data_file = './data/glove.840B.300d.txt'
    embeddings = WordEmbedding(data_file, 'UTF8')

    # Replace the text here to extract the most similar word using word-vectors
    text = "i went to a german restaurant"
    ref_words = ["mexican", "chinese", "french", "british", "american",
                 "japanese", "korean", "italian", "thai", "spanish"]

    tokens = text.split(' ')

    all_words = list(tokens)
    all_words.extend(ref_words)

    vec_dict = embeddings.load_word_vectors(all_words)

    ref_l = []
    for ref in ref_words:
        ref_l.append(vec_dict[ref.encode('utf8')])

    W = np.array(ref_l)

    max_sofar = -1
    word_sofar = None
    for word in tokens:
        w_vec = vec_dict[word.encode('utf8')]
        cosine = np.mean(np.dot(W, w_vec.reshape(-1, 1)))
        if cosine > max_sofar:
            word_sofar = word
            max_sofar = cosine
        print('{:>15} {:>15}'.format(word, np.mean(cosine)))

    print('The most similar word seems to be {}'.format(word_sofar))


if __name__ == '__main__':
    main()
